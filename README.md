|     NRP    |     Nama    |
| :--------- |:--------    |
| 5025201076 | Raul Ilma Rajasa |
| 5025201163 | Muhammad Afdal Abdallah |
| 5025201202 | Aiffah Kiysa Waafi |


# soal shift sisop modul 3 E12 2022

# Soal 1
Permintaan pada nomor satu berfokus pada pembuatan thread dan menjalankan dua proses secara bersamaan. Pembuatan thread untuk setiap point sama saja, yang membedakan yaitu fungsi yang dipassing ke dalam pthread_create.
```
    int err;
    for(int i=0;i<2;i++){
        err = pthread_create(&(tid[i]),NULL,&processE, NULL);
        if(err!=0){
            printf("\n Tidak bisa membuat thread [%s]",strerror(err));
        }else  printf("\n Sukses membuat thread!\n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

```
Kode di atas merupakan potongan kode untuk membuat thread, karena setiap point proses yang dilakukan secara bersamaan yaitu dua proses, maka ukuran array thread cukup 2, dan looping sebanyak dua kali. Fungsi yang dipassing ke pthread_create berubah untuk setiap poin, contoh di atas merupakan pembuatan thread untuk poin B. Cara menjalankan thread yaitu dengan memanggil pthread_join.
```
pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        int status;
        if(fork() == 0){
            char *argv[] = {"unzip", "-q","-o", "-P",pass, "../hasil.zip", "-d", "../", NULL};
            execv("/bin/unzip", argv);
        }
        while(wait(&status) > 0);
    }else if(pthread_equal(id, tid[1])){
        FILE *fptr = fopen("../hasil/no.txt","w");
        fprintf(fptr,"No\n");
        fclose(fptr);
    }

```
Kode di atas yaitu salah satu contoh fungsi proses thread (point E). Hal yang perlu diperhatikan yaitu kondisional di dalam fungsi. Banyaknya kondisional bergantung pada bannyaknya proses yang ingin dijalankan secara bersamaan, karena hanya terdapat dua proses, maka hanya terdapat dua kondisional. Syarat pada kondisional merupakan nilai dari array thread, dimana ketika nilai tersebut bernilai sama dengan id  dari thread yang dijalankan (pthread_self).

Proses untuk point lainnya sama saja, hal yang membedakan yaitu fungsi yang dipassing di dalam pthread_create() dan fungsi prosessnya. Untuk potongan kode proses hanya berisi fork dan execv sesuai dengan permintaan soal.

Fungsi decode pada solusi mengikuti referensi github yang ditemukan di google (release 2018) dan tidak dilakukan perubahan apapun (link terdapat di komen).

```
void getPass(){
    register struct passwd *pw;
    uid_t uid = getuid();
    pw = getpwuid(uid);
    char p[101] ="mihinomenest";
    strcat(p,pw->pw_name);
    strcpy(pass,p);
}
```
Fungsi getPass() berfungsi untuk membuat password dengan format mihinomenest[uses], dimana getpwuid(uid) akan mendapatkan user yang sedang mengakses kodingan.

Fungsi main
```
int main(){
    getPass();
    pointA();
    pointB();
    processMove();
    processZip();
    pointE();
    processZip();
}
```

# Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge
sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:  
  
## Penyelesaian Koneksi Client-Server
Untuk mengoneksikan antara client dan server, diperlukan pengaturan terhadap socket maupun local networking configuration untuk IP server serta port-nya. Pada program akan mengimplementasikan sistem tersebut melalui template yang sudah diberikan pada modul, yaitu seperti berikut:  
Pada client.c:
```
    struct sockaddr_in alamatServer;
    char *login="login",*logout="logout",*regist="register";
    char temp,buffer[1100]={0},cmd[1100],username[1100],password[1100];
    if((sock=socket(AF_INET, SOCK_STREAM, 0))<0)
    {
        printf("Failed to create socket\n");
        return -1;
    }
    memset(&alamatServer, '0', sizeof(alamatServer));
    alamatServer.sin_family=AF_INET,alamatServer.sin_port=htons(port);
    if(inet_pton(AF_INET,"127.0.0.1",&alamatServer.sin_addr)<= 0)
    {
        printf("Invalid address\n"); return -1;
    }
    if(connect(sock,(struct sockaddr *)&alamatServer,sizeof(alamatServer))<0)
    {
        printf("Connection failed\n"); return -1;
    }
```
Pada server.c:
```
    struct sockaddr_in alamat;
    int serverSocket,serverFD,readerConn,opt=1,jumlahKoneksi=5,porter=8080,addr_len = sizeof(alamat);
    if((serverFD=socket(AF_INET,SOCK_STREAM,0))==0) perror("socket failed"),exit(EXIT_FAILURE);
    if(setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,&opt,sizeof(opt))) perror("setsockopt"),exit(EXIT_FAILURE);
    alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
    if(bind(serverFD, (struct sockaddr *) &alamat, sizeof(alamat))<0) perror("bind failed"),exit(EXIT_FAILURE);
    if(listen(serverFD,jumlahKoneksi)<0) perror("listen"),exit(EXIT_FAILURE);
```
a. Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu
server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client.  
Username dan password
memiliki kriteria sebagai berikut:  
● Username unique (tidak boleh ada user yang memiliki username yang sama)  
● Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:
## users.txt
username:password  
username2:password2
  
## Penyelesaian 2a:
Untuk menyelesaikan problem 2a, diperlukan validation pada string yang diinputkan oleh client yang akan diproses dengan server untuk dimasukkan ke dalam database user di server.  
## Register
Dalam client, kita hanya perlu mengambil input dan mengirim buffer input ke server:
```
        else if(same(cmd,regist))
        {
            send(sock,regist,strlen(regist), 0); memset(buffer,0,sizeof(buffer));
            printf("Register\nUsername: ");
            scanf("%c",&temp); scanf("%[^\n]", username);
            send(sock,username,1100,0);
            printf("Password: ");
            scanf("%c",&temp); scanf("%[^\n]", password);
            send(sock,password,1100,0);
            readerConn = read(sock, buffer, 1100);
            if(same(buffer, "RegError")) printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
            if(same(buffer, "Register berhasil")) printf("Register berhasil\n");
            memset(buffer, 0, sizeof(buffer));
        }
```
Pada server.c akan dilakukan pengolahan string dari buffer yang dikirimkan melalui client. Proses pengolahan tersebut, memerlukan sistem file I/O untuk membaca data yang ada di users.txt yang ada pada server, kemudian tinggal dilakukan penglasifikasian data input berupa username yang unik dan kriteria unik dari password dengan logika sederhana sebagai berikut:
```
        else if(same(regist,buffer))
        {
            fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
            readerConn = read(serverSocket, user.name, 1100),
            readerConn = read(serverSocket, user.password, 1100);
            int flag = 0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int check=0,numUsers=0;
            while((fileReader = getline(&line, &len, fp3) != -1))
            {
                numUsers++;
                char userName[105],userPass[105];
                int i = 0;
                while(line[i]!=':') userName[i] = line[i],i++;
                userName[i] = '\0';
                i++;
                int j = 0;
                while(line[i]!='\n') userPass[j] = line[i],j++,i++;
                userPass[j] = '\0';
                if(strcmp(user.name, userName)==0) {check = 0; break;}
                int kapital=0,lower=0,angka=0,jumlah=0;
                for(int i=0;i<strlen(user.password);i++)
                {
                    if(user.password[i]>=48 && user.password[i]<=57)angka=1;
                    if(user.password[i]>=65 && user.password[i]<=90)kapital=1,jumlah++;
                    if(user.password[i]>=97 && user.password[i]<=122)lower=1,jumlah++;
                }
                if(!angka||!lower||!kapital||jumlah<6) break;
                if(angka&&lower&&kapital&&jumlah>=6) check=1;
            }
            if(check||numUsers==0)
            {
                fprintf(fp2, "%s:%s\n", user.name, user.password);
                send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
            }
            else send(serverSocket, "RegError", strlen("RegError"), 0);
            fclose(fp2);
        }

``` 
## Login
Pada client.c:
```
        if(same(cmd,login))
        {
            send(sock,login,strlen(login), 0);
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", username);
            send(sock, username, 1100, 0);
            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            if(same(buffer, "LoginSuccess"))printf("Login process is success! Welcome aboard! ^o^\n");
            else printf("Login process failed, please try again.\n");
```
Pada server.c:
```
        if (same(login,buffer)) 
        {
            readerConn = read(serverSocket, user.name, 1100);
            readerConn = read(serverSocket, user.password, 1100);
            fp3 = fopen("users.txt", "r");
            int check=0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while((fileReader=getline(&line, &len, fp3)!=-1))
            {
                char userName[105],userPass[105];
                int i=0,j=0;
                while(line[i] != ':') userName[i] = line[i],i++;
                userName[i++] = '\0';
                while(line[i] != '\n') userPass[j] = line[i],j++,i++;
                userPass[j] = '\0';
                if (strcmp(user.name,userName)==0&&strcmp(user.password,userPass)==0)
                {
                    check=1,send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
                    break;
                }
            }
            if(check == 0) printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
            else
            {
                printf("Authorization for Login is Success\n");
```
## Logout
Pada client.c:
```
                    if(same(cmd,logout))
                    {
                        send(sock,logout,1100,0);
                        break;
                    }
```
Pada server.c:
```
        if(same("logout", buffer)) break;
```
b. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File
otomatis dibuat saat server dijalankan.  
  
## Penyelesaian 2b
Untuk menyelesaikan problem 2b, pada server.c, kita perlu mengimplementasikan sistem file I/O untuk men-generate suatu file berekstensi tsv dan menuliskan sesuatu di dalamnya untuk membuatnya. Dalam konteks ini, kita akan menuliskan baris baru dengan `\n` pada file problems.tsv di fungsi main sekaligus membuat folder `problems` pada server.c, yaitu:
```
    mkdir("problems", 0777);
    FILE *fp = fopen("problems.tsv", "a");
    if(!fp) fprintf(fp, "\n"),fclose(fp);
    fclose(fp);
```
c. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:  
● Judul problem (unique, tidak boleh ada yang sama dengan problem lain)  
● Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)  
● Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)  
● Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)  
Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.  
## Penjelasan 2c
Untuk menyelesaikan problem 2c, kita perlu mengambil input pada client.c untuk command `add` sekaligus juga menerima hasil output gacha yang diberikan dari server untuk dimasukkan ke dalam filepath output.txt yang sudah diinputkan client. Kemudian, pada server.c, kita akan menyetor data-data berupa judul problem dan path-file dari file deskripsi, input, maupun output ke dalam file description.txt yang ada pada server sekaligus juga melakukan gacha terhadap deskripsi, input, maupun output pada problem yang telah diinputkan.  
Pada client.c:
```
                    else if(same(cmd,"add"))
                    {
                        send(sock, cmd, 1100, 0);
                        char data[1100],problemName[1100];
                        printf("Judul Problem: ");
                        scanf("\n%[^\n]%*c", problemName);
                        send(sock, problemName, 1100, 0);
                        printf("Filepath description.txt: ");
                        scanf("\n%[^\n]%*c", data);
                        char outputFolder[1100],inputFolder[1100],descFolder[1100],outputPath[1100];
                        strcpy(descFolder,data);
                        removeStr(descFolder,"/description.txt");
                        mkdir(descFolder, 0777);
                        send(sock, data, 1100, 0);
                        printf("Filepath input.txt: ");
                        scanf("\n%[^\n]%*c", data);
                        strcpy(inputFolder,data);
                        removeStr(inputFolder,"/input.txt");
                        mkdir(inputFolder, 0777);
                        send(sock, data, 1100, 0);
                        printf("Filepath output.txt: ");
                        scanf("\n%[^\n]%*c", data);
                        send(sock, data, 1100, 0);
                        readerConn = read(sock, buffer, 1100);
                        int numGacha = atoi(buffer);
                        memset(buffer, 0, sizeof(buffer));
                        strcpy(outputPath,data);
                        FILE *fileManager;
                        fileManager = fopen(outputPath, "a+");
                        for(int i=0;i<numGacha;i++)
                            readerConn = read(sock, buffer, 1100),
                            fprintf(fileManager, "%s", buffer),
                            memset(buffer, 0, sizeof(buffer));
                        fclose(fileManager);
                        printf("Successfully adding problem %s to the server!\n",problemName);
                        continue;
                    }
```
Pada server.c:
```
                    else if(same("add",buffer))
                    {
                        problem request;
                        char basePath[1100],clientPath[1100],descPath[1100],inputPath[1100];
                        char descServer[1100],inputServer[1100],outputServer[1100];
                        readerConn = read(serverSocket, request.problem_title, 1100);
                        printf("Judul problem: %s\n", request.problem_title);
                        readerConn = read(serverSocket, request.pathDesc, 1100);
                        printf("Filepath description.txt: %s\n", request.pathDesc);
                        readerConn = read(serverSocket, request.pathInput, 1100);
                        printf("Filepath input.txt: %s\n", request.pathInput);
                        readerConn = read(serverSocket, request.pathOutput, 1100);
                        printf("Filepath output.txt: %s\n", request.pathOutput);
                        strcpy(basePath, "problems/");
                        strcat(basePath, request.problem_title);
                        mkdir(basePath, 0777);
                        strcat(basePath, "/");
                        strcpy(descServer, basePath);
                        strcat(descServer, "description.txt");
                        strcpy(inputServer, basePath);
                        strcat(inputServer, "input.txt");
                        strcpy(outputServer, basePath);
                        strcat(outputServer, "output.txt");
                        srand(time(0));
                        int gachaTestcase = rand()%50,gachaInput;
                        if(gachaTestcase<10)gachaTestcase+=5;
                        FILE *data;
                        if(!(data = fopen(descServer, "r")))
                            data = fopen(descServer, "a+"),
                            fprintf(data, "%s by %s\nDescription:\n", request.problem_title, user.name),
                            fprintf(data, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lorem leo, malesuada in ultricies ac, volutpat id dui.\n"),
                            fprintf(data, "Filepath description.txt: %s\nFilepath input.txt: %s\nFilepath output.txt: %s\n",request.pathDesc,request.pathInput,request.pathOutput),
                            fclose(data);
                        if(!(data = fopen(inputServer, "r")))
                        {
                            data = fopen(inputServer, "a+");
                            fprintf(data,"%d\n", gachaTestcase);
                            for(int i=0;i<gachaTestcase;i++) gachaInput = rand()%150,fprintf(data, "%d ", gachaInput);
                            fprintf(data,"\n");
                            fclose(data);
                        }
                        int res[gachaTestcase],gachaOutput;
                        if(!(data = fopen(outputServer, "r")))
                        {
                            data = fopen(outputServer, "a+");
                            for(int i=0;i<gachaTestcase;i++) gachaOutput = rand()%1000, res[i]=gachaOutput,fprintf(data, "Case #%d: %d\n", i+1, gachaOutput);
                            fclose(data);
                        }
                        char gachaTC[1100];
                        sprintf(gachaTC,"%d",gachaTestcase);
                        send(serverSocket, gachaTC, sizeof(gachaTC), 0);
                        int gachaOut=rand()%500,gachaResult = 0;
                        if(gachaOut%2==0) gachaResult=1; // kalau genap AC, ganjil WA (nilai default)
                        char outputForClient[1100];
                        sprintf(outputForClient,"Case #1: %d\n",gachaResult?res[0]:res[0]+5);
                        send(serverSocket, outputForClient, 1100, 0);
                        memset(outputForClient,0,sizeof(outputForClient));
                        for(int i=1;i<gachaTestcase;i++) 
                            sprintf(outputForClient, "Case #%d: %d\n", i+1, res[i]),
                            send(serverSocket, outputForClient, 1100, 0),
                            memset(outputForClient,0,sizeof(outputForClient));
                        fp = fopen("problems.tsv", "a");
                        fprintf(fp, "%s\t%s\n", request.problem_title, user.name),fclose(fp);
                        printf("%s is adding new problem called %s to the server.\n",user.name,request.problem_title);
                        continue;
                    }
```
d. Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:  
[judul-problem-1] by [author1]  
[judul-problem-2] by [author2]  
  
## Penyelesaian 2d
Untuk menyelesaikan problem 2d, selain melakukan I/O dari client yang dikirimkan melalui buffer ke server, kita juga perlu melakukan metode pembacaan file (file read) pada server di file problems.tsv untuk menampilkan format yang sesuai pada problem 2d. Lalu, pada server, kita akan mengirimkan output berupa string yang sesuai dengan kriteria soal, yaitu [judul-problem] by [author]. Kemudian, pada client, kita tinggal meretrieve data buffer yang dikirimkan dari server untuk dapat ditampilkan ke dalam terminal client.  
Pada client.c:
```
                    else if(same(cmd,"see"))
                    {
                        printf("Problems Available in E12 Online Judge:\n");
                        send(sock,cmd,1100,0); memset(buffer,0,sizeof(buffer));
                        while(readerConn = read(sock, buffer, 1100))
                        {
                            if(same(buffer, "e")) break;
                            printf("%s", buffer);
                        }
                        continue;
                    }
```
Pada server.c:
```
                    else if(same("see", buffer))
                    {
                        fp=fopen("problems.tsv", "r");
                        if(!fp)
                        {
                            send(serverSocket, "e", sizeof("e"), 0);
                            memset(buffer, 0, sizeof(buffer));
                            continue;
                        }
                        char *line = NULL;
                        ssize_t len = 0;
                        ssize_t fileReader;
                        while((fileReader=getline(&line,&len,fp)!=-1))
                        {
                            problem temp_problem;
                            storeProblemTSV(&temp_problem, line);
                            char message[1100];
                            sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
                            send(serverSocket, message, 1100, 0);
                        }
                        send(serverSocket, "e", sizeof("e"), 0),fclose(fp);
                        memset(buffer, 0, sizeof(buffer));
                    }
```
e. Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.  
  
## Penyelesaian 2e
Untuk menyelesaikan problem 2e, kita perlu membaca data dari client yang berupa command `download` dengan parameter <judul-problem> yang kemudian akan kita kirimkan menjadi buffer ke server. Setelah itu, server akan mengolah informasi buffer dari client, sehingga pada server, kita hanya perlu melakukan perpindahan isi file description.txt maupun input.txt dari server ke filepath problem client yang sudah diatur ketika menginputkan problem ke dalam server. Sehingga, ketika server sudah membaca dan mengirimkan informasi berupa isi file tersebut, di client, kita tinggal menuliskan isi file tersebut ke dalam direktori file path yang sudah diatur sebelumnya untuk description.txt maupun input.txt.  
Pada client.c:
```
                    else if(same(cmd,"download"))
                    {
                        send(sock, cmd, 1100, 0);
                        memset(buffer, 0, sizeof(buffer));
                        scanf("%s", cmd);
                        send(sock, cmd, 1100, 0);
                        readerConn = read(sock, buffer, 1100);
                        int numLine = atoi(buffer);
                        char inputTarget[1100];
                        readerConn = read(sock, inputTarget, 1100);
                        for(int i=0;i<numLine;i++)
                        {
                            FILE *fpClient=fopen(inputTarget, "r");
                            fpClient = fopen(inputTarget, "a+");
                            char fileContent[1100];
                            readerConn = read(sock, fileContent, 1100);
                            fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
                        }
                        memset(buffer, 0, sizeof(buffer));
                        readerConn = read(sock, buffer, 1100);
                        char fileReady[] = "File is ready to be downloaded.\n",descTarget[1100];
                        if(same(buffer, fileReady)) 
                        {
                            memset(buffer, 0, sizeof(buffer));
                            readerConn = read(sock, buffer, 1100);
                            strcpy(descTarget, buffer);
                            printf("Downloading description & input from problem %s..\n", cmd);
                            int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
                            if(!des_fd) perror("can't open file"), exit(EXIT_FAILURE);
                            int file_read_len;
                            char buff[1100];
                            while(1) 
                            {
                                memset(buff, 0, 1100);
                                file_read_len = read(sock, buff, 1100);
                                write(des_fd, buff, file_read_len);
                                break;
                            }
                            printf("Successfully downloading the files for problem %s to the desired client directory!\n",cmd);
                        }
                        memset(buffer, 0, sizeof(buffer));
                        continue;
                    }
```
Pada server.c:
```
                    else if(same("download",buffer))
                    {
                        readerConn = read(serverSocket, buffer, 1100);
                        bool foundDesc = false, foundInput = false;
                        char pathDownload[1100] = "problems/",descPath[1100],descTarget[1100];
                        strcat(pathDownload, buffer);
                        strcpy(descPath, pathDownload);
                        strcat(descPath, "/description.txt");
                        char inputPath[1100],inputTarget[1100],outputPath[1100],outputTarget[1100];
                        strcpy(inputPath, pathDownload);
                        strcat(inputPath, "/input.txt");
                        strcpy(outputPath, pathDownload);
                        strcat(outputPath, "/output.txt");
                        char foundSuccess[] = "File is ready to be downloaded.\n",notFound[] = "No such file found.\n";
                        fp = fopen(descPath, "r");
                        FILE *source, *target;
                        source = fopen(descPath, "r");
                        char *line = NULL,*line2=NULL,*line3=NULL;
                        ssize_t len = 0;
                        ssize_t fileReader;
                        int num=0,fd,readLength;
                        while((fileReader=getline(&line,&len,source)!=-1))
                        {
                            num++;
                            if(num>3)
                            {
                                char pathContent[105];
                                int i=0,j=0;
                                while(line[i]!=':') i++;
                                line[i]='\0'; i+=2;
                                while(line[i] != '\n') pathContent[j++]=line[i++]; pathContent[j] = '\0';
                                if(num==4) strcpy(descTarget,pathContent);
                                if(num==5) strcpy(inputTarget,pathContent);
                                if(num==6) strcpy(outputTarget,pathContent);
                            }
                        }
                        int numLine=0;
                        FILE *fpSource= fopen(inputPath, "r");
                        ssize_t len2 = 0;
                        ssize_t fileReader2;
                        bool checkSubmission = true;
                        while((fileReader2=getline(&line2,&len2,fpSource)!=-1)) numLine++;
                        fclose(fpSource);
                        fpSource = fopen(inputPath, "r");
                        char buf[1100];
                        sprintf(buf, "%d", numLine);
                        send(serverSocket, buf, 1100, 0);
                        send(serverSocket, inputTarget, 1100, 0);
                        ssize_t len3=0;
                        ssize_t fileReader3;
                        while((fileReader3=getline(&line3,&len3,fpSource)!=-1))
                        {
                            char outputSource[105];
                            int i=0;
                            while(line3[i]!='\n') outputSource[i]=line3[i],i++;
                            outputSource[i] = '\0';
                            send(serverSocket, outputSource, 1100, 0);
                        }
                        fclose(fpSource);
                        if(fp) foundDesc = true;
                        if(!foundDesc) send(serverSocket, notFound, 1100, 0);
                        else
                        {
                            send(serverSocket, foundSuccess, 1100, 0);
                            send(serverSocket, descTarget, 1100, 0);
                            fd = open(descPath, O_RDONLY);
                            if(!fd) perror("Fail to open"), exit(EXIT_FAILURE);
                            while(1) 
                            {
                                memset(descPath, 0x00, 1100);
                                readLength = read(fd, descPath, 1100);
                                if(readLength == 0) break;
                                else send(serverSocket, descPath, readLength, 0);
                            }
                            close(fd);
                        }
                        fclose(fp);
                    }
```
f. Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file
output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang
dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.  
  
## Penyelesaian 2f:
Untuk menyelesaikan problem 2f, kita perlu mengidentifikasi input dari client berupa command `submit` dan parameter <judul-problem> + <path-file-output.txt> yang kemudian kita kirimkan sebagai buffer kepada server. Setelah itu kita akan melakukan comparison (perbandingan) terhadap file output.txt yang ada pada server dengan file output.txt yang ada pada client. Jika terdapat perbedaan dari masing-masing test-case yang ada pada server-client, maka hasil yang akan dikeluarkan adalah `WA`. Sebaliknya, jika sama total output dari server dengan client, maka hasil yang dikeluarkan adalah `AC`.  
Pada client.c:
```
                    else if(same(cmd,"submit"))
                    {
                        send(sock, cmd, 1100, 0);
                        memset(buffer, 0, sizeof(buffer));
                        char namaProblem[1100],filePath[1100];
                        scanf("%s %s", namaProblem, filePath);
                        send(sock, namaProblem, 1100, 0);
                        FILE *fpC = fopen(filePath, "r");
                        if(!fpC) {printf("Output file is not exist!\n");break;}
                        char *line = NULL;
                        ssize_t len = 0;
                        ssize_t fileReader;
                        bool checkSubmission = true;
                        while((fileReader=getline(&line,&len,fpC)!=-1))
                        {
                            char outputSubmission[105];
                            int i=0,j=0;
                            while(line[i]!=':')i++;
                            line[i++]='\0';
                            while(line[i] != '\n') outputSubmission[j++]=line[i++];
                            outputSubmission[j]='\0';
                            send(sock,outputSubmission, 105, 0);
                        }
                        memset(buffer, 0, sizeof(buffer));
                        readerConn = read(sock, buffer, 1100);
                        printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
                    }
```
Pada server.c:
```
                    else if(same("submit",buffer))
                    {
                        char problemName[1100],pathSource[1100]="problems/";
                        readerConn = read(serverSocket,problemName,1100);
                        strcat(pathSource,problemName);
                        strcat(pathSource,"/output.txt");
                        FILE *fpSource= fopen(pathSource, "r");
                        char *line=NULL;
                        ssize_t len=0;
                        ssize_t fileReader;
                        bool checkSubmission = true;
                        while(fileReader=getline(&line,&len,fpSource)!=-1)
                        {
                            char outputSource[105],outputSubmission[105];
                            int i=0,j=0;
                            while(line[i] != ':')i++;
                            line[i++]='\0';
                            while(line[i] != '\n') outputSource[j++]=line[i++];
                            outputSource[j] = '\0';
                            readerConn = read(serverSocket,outputSubmission,105);
                            int source=atoi(outputSource),submit=atoi(outputSubmission);
                            printf("[%s] source: %d dan submission: %d\n",submit==source?"AC":"WA",source,submit);
                            if(submit!=source) checkSubmission = false;
                        }
                        printf("%s is %s on problem %s!\n",user.name,checkSubmission?"accepted (AC)":"having a wrong answer (WA)",problemName);
                        send(serverSocket, checkSubmission?"AC\nAccepted in Every Testcases\n":"WA\nWA in the First Testcase\n", 1100, 0);
                    }
```
g. Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.  
  
## Penyelesaian 2g
Untuk menyelesaikan problem 2g, kita perlu mengonfigurasikan network session dari server untuk terkelola berdasarkan jumlah koneksi yang bersifat plural sehingga dapat menangani multiple-connection. Lalu, pada server, kita hanya perlu mengidentifikasi jumlah client yang sedang terkoneksi pada server dan mengirimkan informasi tersebut ke dalam client sehingga ketika terdapat client yang masih mengakses online judge kita, maka otomatis client lainnya harus menunggu hingga tidak ada client yang mengakses online-judge di saat yang sama.  

## Hasil Program soal2:
![image-1.png](https://drive.google.com/uc?export=view&id=1g0-tCp6326hzDitP7pcg5jAfx3_046gU)
![image-2.png](https://drive.google.com/uc?export=view&id=1Dpxuy04BmPrsEVr2CoMACEF1As1ktrYX)
![image-3.png](https://drive.google.com/uc?export=view&id=1RndEiJ3DfSjAhMUvYuTERhP8sGVU61uT)
![image-4.png](https://drive.google.com/uc?export=view&id=1E9R34rljw88BRMNYAh8djTto5hR0qwpP)
![image-5.png](https://drive.google.com/uc?export=view&id=14E4G7aLQXuUvkO0lgK4M9yw_OKqAuNyT)
![image-6.png](https://drive.google.com/uc?export=view&id=1z0uN3LpYhCmTrh58BFcaw9Q4jwV5uyUn)
![image-7.png](https://drive.google.com/uc?export=view&id=1di7iJSpfeb5VpIY45q4rT24xW_o0lMSO)
![image-8.png](https://drive.google.com/uc?export=view&id=1Snm6W9NvGdhRyq3-9IH58VbxOqyhIYCC)
![image-9.png](https://drive.google.com/uc?export=view&id=1UdXwjVRrsvwhqojcfKyLv5Pbz0XIZO9v)
![image-10.png](https://drive.google.com/uc?export=view&id=1HpQUDK3AJ7uF1LLCOksntDrF0Ulb7syQ)
![image-11.png](https://drive.google.com/uc?export=view&id=1Q7QSY5jmDbT1oFFNjugS-Y5934HG3UsZ)
![image-12.png](https://drive.google.com/uc?export=view&id=1ztB8ua_bZ9XRER26MKN2aIsHjhcvqMYB)
![image-13.png](https://drive.google.com/uc?export=view&id=1HQ_6etrXnv96RzUhQbir7-470XsyG69V)
![image-14.png](https://drive.google.com/uc?export=view&id=1EizLavaHJuRPaJDkmcqjOtKgPPpTWvit)
![image-15.png](https://drive.google.com/uc?export=view&id=13GtCiXmwpq6ebQQPgYcSth8ynmFbz_GZ)
![image-16.png](https://drive.google.com/uc?export=view&id=1isz7elceK8HjhFZ8iaKKR7DaKle6EURV)
![image-17.png](https://drive.google.com/uc?export=view&id=1bV3wGbftn5MyEuL4GKHT4RZdUmQIOSqQ)
![image-18.png](https://drive.google.com/uc?export=view&id=1eXQVfgneScdEX6g-ElU5fI_raOAZo29M)
![image-19.png](https://drive.google.com/uc?export=view&id=1I5IcqjRdcFhAM0Si3UIQEk5u-2kROf-T)
![image-20.png](https://drive.google.com/uc?export=view&id=1i8E403ud04Y9EQ30B2w5DpKWfElN1Bo7)
