#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <pwd.h>

pthread_t tid[2];

char map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};



char pass[101];
void getPass(){
    register struct passwd *pw;
    uid_t uid = getuid();
    pw = getpwuid(uid);
    char p[101] ="mihinomenest";
    strcat(p,pw->pw_name);
    strcpy(pass,p);
}

// https://github.com/elzoughby/Base64 (May 2018)
// Referensi decode base64
void decodeText(char str[]){
    int counts = 0;
    char buffer[4];
    char res[1001]; 
    int i = 0, p = 0;

    for(i = 0; str[i] != '\0'; i++) {
        int k;
        for(k = 0 ; k < 64 && map[k] != str[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            res[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                res[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                res[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    res[p] = '\0';
    strcpy(str, res);
}

void* processA(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        int status;
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"mkdir", "-p", "../quote", NULL};
   		    execv("/bin/mkdir", argv);
        }else{
            while(wait(&status) > 0);
            int status2;
            pid_t child2 = fork();
            if(child2 == 0){
                char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", 
                "-O","quote.zip"};
   		        execv("/bin/wget", argv);
            }else{
                while(wait(&status2) > 0);
                int status3;
                pid_t child3 = fork();
                if(child3 == 0){
                    char *argv[] = {"unzip", "-q", "./quote.zip", "-d", "../quote", NULL};
                    execv("/bin/unzip", argv);
                }
                while(wait(&status3) > 0);
            }
        }
    }else if(pthread_equal(id, tid[1])){
        int status;
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"mkdir", "-p", "../music", NULL};
   		    execv("/bin/mkdir", argv);
        }else{
            while(wait(&status) > 0);
            int status2;
            pid_t child2 = fork();
            if(child2 == 0){
                char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", 
                "-O","music.zip"};
   		        execv("/bin/wget", argv);
            }else{
                while(wait(&status2) > 0);
                int status3;
                pid_t child3 = fork();
                if(child3 == 0){
                    char *argv[] = {"unzip", "-q", "./music.zip", "-d", "../music", NULL};
                    execv("/bin/unzip", argv);
                }
                while(wait(&status3) > 0);
            }
        }
    }
}

void pointA(){
    int err;
    for(int i=0;i<2;i++){
        err = pthread_create(&(tid[i]),NULL,&processA, NULL);
        if(err!=0){
            printf("\n Tidak bisa membuat thread [%s]",strerror(err));
        }else  printf("\n Sukses membuat thread!\n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}

void* processB(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        char path[] = "../quote/";
        DIR *dir = opendir(path);
        struct dirent *dt;
        FILE *fptr;
        fptr = fopen("../quote/quote.txt","a");
        if(fptr == NULL){
            printf("ERROR!\n");
            exit(1);
        }
        while ((dt = readdir(dir)) != NULL){
            if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "quote.txt") != 0){
                char filepath[] = "../quote/";
                strcat(filepath, dt->d_name);
                FILE *text = fopen(filepath, "r");
                if(text == NULL){
                    printf("ERROR!\n");
                    continue;
                }
                char str[1001];
                fgets(str, sizeof(str),text);
                fclose(text);
                long len = strlen(str);
                decodeText(str);
                fprintf(fptr, "%s\n",str);
            }
        }
        fclose(fptr);
        closedir(dir);
    }else if(pthread_equal(id, tid[1])){
        char path[] = "../music/";
        DIR *dir = opendir(path);
        struct dirent *dt;
        FILE *fptr;
        fptr = fopen("../music/music.txt","a");
        if(fptr == NULL){
            printf("ERROR!\n");
            exit(1);
        }
        while ((dt = readdir(dir)) != NULL){
            if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "music.txt") != 0){
                char filepath[] = "../music/";
                strcat(filepath, dt->d_name);
                FILE *text = fopen(filepath, "r");
                if(text == NULL){
                    printf("ERROR!\n");
                    continue;
                }
                char str[1001];
                fgets(str, sizeof(str),text);
                fclose(text);
                // printf("%s\n", str);
                long len = strlen(str);
                decodeText(str);
                // printf("decoded: %s\n", str);
                fprintf(fptr, "%s\n",str);
            }
        }
        fclose(fptr);
        closedir(dir);
    }
}

void pointB(){
    int err;
    for(int i=0;i<2;i++){
        err = pthread_create(&(tid[i]),NULL,&processB, NULL);
        if(err!=0){
            printf("\n Tidak bisa membuat thread [%s]",strerror(err));
        }else  printf("\n Sukses membuat thread!\n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}

void* processE(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        int status;
        if(fork() == 0){
            char *argv[] = {"unzip", "-q","-o", "-P",pass, "../hasil.zip", "-d", "../", NULL};
            execv("/bin/unzip", argv);
        }
        while(wait(&status) > 0);
    }else if(pthread_equal(id, tid[1])){
        FILE *fptr = fopen("../hasil/no.txt","w");
        fprintf(fptr,"No\n");
        fclose(fptr);
    }
}

void pointE(){
    int err;
    for(int i=0;i<2;i++){
        err = pthread_create(&(tid[i]),NULL,&processE, NULL);
        if(err!=0){
            printf("\n Tidak bisa membuat thread [%s]",strerror(err));
        }else  printf("\n Sukses membuat thread!\n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}


void processMove(){
    int status = 0;
   	if(fork() == 0){
        char *argv[] = {"mkdir", "-p", "../hasil", NULL};
   		execv("/bin/mkdir", argv);
    }else{
        while(wait(&status) > 0);
        int status2;
        if(fork() == 0){
            char *argv[] = {"find", "../quote", "-name", "quote.txt", "-exec", "mv", "-t","../hasil", "{}","+",NULL };
            execv("/bin/find", argv);
        }else{
            while(wait(&status2) > 0);
            int status3;
            if(fork() == 0){
                char *argv[] = {"find", "../music", "-name", "music.txt", "-exec", "mv", "-t","../hasil", "{}","+",NULL };
                execv("/bin/find", argv);   
            }
            while(wait(&status3)>0);
        }
    }
}

void processZip(){
    int status;
    if(fork() == 0){
        char *argv[] = {"zip", "-P", pass,"-r","../hasil.zip","-q",
        "../hasil", NULL};
   		execv("/bin/zip", argv);
    }
}

int main(){
    getPass();
    pointA();
    pointB();
    processMove();
    processZip();
    pointE();
    processZip();
}
